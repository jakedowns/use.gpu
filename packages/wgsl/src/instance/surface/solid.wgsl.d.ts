declare module "@use-gpu/wgsl/instance/surface/solid.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getSolidSurface: ParsedBundle;
  export default __module;
}
