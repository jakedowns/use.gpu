declare module "@use-gpu/wgsl/instance/surface/normal-map.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getNormalMapSurface: ParsedBundle;
  export default __module;
}
