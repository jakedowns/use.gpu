declare module "@use-gpu/wgsl/instance/vertex/dual-contour.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getDualContourVertex: ParsedBundle;
  export default __module;
}
