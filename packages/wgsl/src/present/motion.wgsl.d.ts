declare module "@use-gpu/wgsl/present/motion.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getSlideMotion: ParsedBundle;
  export default __module;
}
