declare module "@use-gpu/wgsl/mask/point.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const circle: ParsedBundle;
  export const diamond: ParsedBundle;
  export const square: ParsedBundle;
  export const circleOutlined: ParsedBundle;
  export const diamondOutlined: ParsedBundle;
  export const squareOutlined: ParsedBundle;
  export default __module;
}
