declare module "@use-gpu/wgsl/material/pbr-apply.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const applyPBRMaterial: ParsedBundle;
  export default __module;
}
