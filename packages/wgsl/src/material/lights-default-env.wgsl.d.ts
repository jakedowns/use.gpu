declare module "@use-gpu/wgsl/material/lights-default-env.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getDefaultEnvironment: ParsedBundle;
  export default __module;
}
