declare module "@use-gpu/wgsl/pmrem/pmrem-copy.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const pmremCopy: ParsedBundle;
  export default __module;
}
