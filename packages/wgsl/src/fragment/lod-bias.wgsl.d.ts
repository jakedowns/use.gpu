declare module "@use-gpu/wgsl/fragment/lod-bias.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getLODBiasedTexture: ParsedBundle;
  export default __module;
}
