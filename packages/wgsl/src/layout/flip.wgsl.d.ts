declare module "@use-gpu/wgsl/clip/flip.wgsl" {
  type ParsedBundle = import('@use-gpu/shader/wgsl/types').ParsedBundle;
  const __module: ParsedBundle;
  export const getFlippedPosition: ParsedBundle;
  export default __module;
}
